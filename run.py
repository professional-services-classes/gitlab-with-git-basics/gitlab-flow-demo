# Compliments of Fern
import os
from notes import note



# AWS creds in broad daylight!
aws_key_id = "AWSKEYMADEBYSOMEONEXXXXXXX"
aws_key_secret = "AWSKEYMSECRETMADEBYSOMEONEXXXXXXX"


if __name__ = "__main__":
    port = int(os.environ.get("PORT",9090))
    note.run(host='0.0.0.0', port=port)
