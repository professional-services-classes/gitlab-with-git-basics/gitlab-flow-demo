**Gitlab Hands On Labs** 

Complete all 6 Labs using the [attached guide](https://gitlab-core.us.gitlabdemo.cloud/training-sample-projects/ps-classes/gitlab-with-git-basics/gitlab-flow-demo/-/blob/master/GitLab_with_Git_Basics_Hands_On_Guide.pdf)

- [ ] GITLAB BASICS LAB SETUP	
- [ ] LAB 1- CREATE A PROJECT AND ISSUE	
- [ ] LAB 2- WORKING WITH GIT LOCALLY	
- [ ] LAB 3- USING GITLAB ISSUES TO PUSH CODE	
- [ ] LAB 4- BUILD A GITLAB-CI.YML FILE	
- [ ] LAB 5- AUTO DEVOPS LAB- PREDEFINED TEMPLATE	
- [ ] LAB 6- SECURITY SCANNING (SAST)	


